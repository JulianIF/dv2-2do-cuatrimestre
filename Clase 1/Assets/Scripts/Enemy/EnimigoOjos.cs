﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnimigoOjos : MonoBehaviour {

    // Use this for initialization
    public GameObject Player;
	public GameLogic game;
    private bool enRango = false;
	void Start () {
        
       
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 diff = Player.transform.position - this.transform.position;

        diff.y = 0.0f;
        diff.Normalize();

        RaycastHit hit;

        if (Vector3.Angle(this.transform.forward, diff) < 45.0f)
        {
            if (Physics.Raycast(this.transform.position, diff, out hit))
            {
                if (hit.collider.tag == "Player" && enRango)
                {
					game.PlayerLost ();
                    enRango = false;
                }
            }
        }


    }
    void OnTriggerEnter (Collider collider) 
	{
		if (collider.CompareTag("Player") && !collider.CompareTag("Obstacle"))
        {
            enRango = true;
        }
    }
}
