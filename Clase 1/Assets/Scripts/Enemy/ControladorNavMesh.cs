using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControladorNavMesh : MonoBehaviour {


    [HideInInspector]
    public Transform perseguirObjectivo;
	UnityEngine.AI.NavMeshAgent navMeshAgent;

//	private navMeshAgent navMeshAgent;
	public float velocidad;
	public Transform objetivo;
	public Transform objetivo2;
	public float vel_rotacion;
	private Transform enemigo;
	private Vector3 Pos;
	private Vector3 puntoDestino;

	void Start () {
		//navMeshAgent = GetComponent < NevMeshAgent> ();
		objetivo = GameObject.Find ("Point1").transform;
		objetivo2 = GameObject.Find ("Point2").transform;
		enemigo = GameObject.Find ("Enemigo").transform;
	}
	void Awake () {
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		navMeshAgent.SetDestination (objetivo.position);
	}
	
	public void ActualizarPuntoDestinoNavMeshAgent(Vector3 puntoDestino) {
        navMeshAgent.destination = puntoDestino;
		this.puntoDestino = puntoDestino;
       // navMeshAgent.Resume();
    }

    public void ActualizarPuntoDestinoNavMeshAgent()
    {
        ActualizarPuntoDestinoNavMeshAgent(perseguirObjectivo.position);
    }

    public void DetenerNavMeshAgent()
    {
        navMeshAgent.Stop();
    }

    public bool HemosLlegado()
    {
        return navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending;
    }

	void Update(){
		
		if(Vector3.Distance(transform.position, objetivo.position)<= 3)
		{
			navMeshAgent.SetDestination (objetivo2.position);
		}
		if(Vector3.Distance(transform.position, objetivo2.position)<= 3)
		{
			navMeshAgent.SetDestination (objetivo.position);
		}
		if(Vector3.Distance(transform.position, puntoDestino)<= 3)
		{
			navMeshAgent.SetDestination (objetivo.position);
		}

	}
}
