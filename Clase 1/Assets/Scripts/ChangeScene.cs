﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour 
{
	public GameObject LoadingScreen;
	string sceneToLoad;
	bool loadScene = false;
	int count = 0;
	// Update is called once per frame
	public void ChangeToScene (string nextScene) 
	{
		if (!loadScene) {
			sceneToLoad = nextScene;
			LoadingScreen.SetActive (true);
			loadScene = true;
		}
		//SceneManager.LoadScene (nextScene);
	}
	public void ExitGame ()
	{
		Application.Quit ();
	}

	void Update()
	{
		if (loadScene) {
			count++;
			if (count > 2) {
				count = 0;
				loadScene = false;
				SceneManager.LoadScene (sceneToLoad);
			}
		}
	}
}
