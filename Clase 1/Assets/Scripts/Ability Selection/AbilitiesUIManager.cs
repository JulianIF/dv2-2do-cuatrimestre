﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitiesUIManager : MonoBehaviour
{
	public Toggle[] Abilities;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		int count = 0;
		for (int i = 0; i < Abilities.Length; i++) {
			if (Abilities [i].isOn)
				count++;
		}

		if (count >= 2) {
			for (int i = 0; i < Abilities.Length; i++) {
				if (!Abilities [i].isOn)
					Abilities [i].interactable = false;
			}
		}
		else{
			for (int i = 0; i < Abilities.Length; i++) {
				Abilities [i].interactable = true;
			}
		}
	}
}

