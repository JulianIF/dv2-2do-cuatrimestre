﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitySelected : MonoBehaviour
{
	public enum Abilities
	{
		Hack,
		Key,
		Rock,
		Smoke
	}

	public Abilities Ability;

	public Image I1;
	public Image I2;

	public Sprite imagen1;
	public Sprite imagen2;
	public Sprite imagen3;
	public Sprite imagen4;
	public Sprite imagen5;

	Toggle button;

	// Use this for initialization
	void Start () 
	{
		StaticVariables.hack = false;
		StaticVariables.rock = false;
		StaticVariables.key = false;
		StaticVariables.smoke = false;
		button = GetComponent<Toggle> ();

		button.onValueChanged.AddListener (OnValueChange);
	}
	
	public void OnValueChange(bool val)
	{
		switch (Ability) 
		{
			case Abilities.Key:
				StaticVariables.key = val;
				if (val) 
				{
					if (I1.sprite.name != imagen5.name && I2.sprite.name == imagen5.name)
						I2.sprite = imagen1;
					else
						I1.sprite = imagen1;
				} 
				else if (I1.sprite.name == imagen1.name)
					I1.sprite = imagen5;
				else if (I2.sprite.name == imagen1.name)
					I2.sprite = imagen5;
						break;

			case Abilities.Hack:
				StaticVariables.hack = val;
				if (val) 
				{
					if (I1.sprite.name != imagen5.name && I2.sprite.name == imagen5.name)
						I2.sprite = imagen2;
					else
						I1.sprite = imagen2;
				} 
				else if (I1.sprite.name == imagen2.name)
					I1.sprite = imagen5;
				else if (I2.sprite.name == imagen2.name)
					I2.sprite = imagen5;
				break;

			case Abilities.Rock:
				StaticVariables.rock = val;
				if (val) 
				{
					if (I1.sprite.name != imagen5.name && I2.sprite.name == imagen5.name)
						I2.sprite = imagen3;
					else
						I1.sprite = imagen3;
				} 
				else if (I1.sprite.name == imagen3.name)
					I1.sprite = imagen5;
				else if (I2.sprite.name == imagen3.name)
					I2.sprite = imagen5;
				break;

			case Abilities.Smoke:
				StaticVariables.smoke = val;
				if (val) 
				{
					if (I1.sprite.name != imagen5.name && I2.sprite.name == imagen5.name)
						I2.sprite = imagen4;
					else
						I1.sprite = imagen4;
				} 
				else if (I1.sprite.name == imagen4.name)
					I1.sprite = imagen5;
				else if (I2.sprite.name == imagen4.name)
					I2.sprite = imagen5;
				break;
		}
	}
}
