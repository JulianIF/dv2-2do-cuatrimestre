﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collect : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider collider) 
	{
		if (collider.CompareTag("Player") && !collider.CompareTag("Obstacle"))
		{
			Destroy (gameObject);
		}
	}
}
