﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wait : Actions {

	private float waitTime;
	private float elapsedTime;
	public Animator animator;
	// Use this for initialization

	public Wait (float t, Animator a)
	{
		waitTime = t;
		elapsedTime = 0;
		animator = a;
	}
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	override public ActionState Execute()
	{
		if (waitTime > elapsedTime) 
		{
			animator.SetBool ("Moving", false);
			animator.SetBool ("Waiting", true);
			elapsedTime += 1 * Time.deltaTime;
			return ActionState.Running;
		} 
		else 
		{
			animator.SetBool ("Waiting", false);
			return ActionState.Finished;
		}
	}
}
