﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : Actions
{
	private Vector3 targetPos;
    private GameObject player;
    private GameObject cam;
    private bool moving;
	public Animator animator;

	public Move (Vector3 t, ref GameObject camPos, Animator a)
	{
		targetPos = t;
        player = GameObject.Find("Player");
        cam = camPos;
        moving = false;
		animator = a;
	}


	override public ActionState Execute()
	{
        player.transform.LookAt(new Vector3(targetPos.x, player.transform.position.y, targetPos.z));
        moving = true;
		animator.SetBool ("Waiting", false);
		animator.SetBool ("Moving", moving);
        if (moving)
        {
            if (Vector3.Distance(player.transform.position, targetPos) < 5f)
            {
                moving = false;
                return ActionState.Finished;
            }
            player.transform.Translate(new Vector3(0, 0, 0.1f));

        }
        return ActionState.Running;
	}
}
