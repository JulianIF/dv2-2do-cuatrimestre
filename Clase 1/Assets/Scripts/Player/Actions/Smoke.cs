﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smoke : Actions
{

    private Vector3 pos;
    private GameObject bomb;
    public Smoke(Vector3 v, GameObject b)
    {
        pos = v;
        bomb = b;
    }
    // Use this for initialization
    void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    override public ActionState Execute()
    {
        GameObject smokeBomb = GameObject.Instantiate(bomb) as GameObject;
        smokeBomb.transform.position = pos;
        return ActionState.Finished;
    }
}
