﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchOff : Actions {

	private CameraController paraApagar;
	private float waitTime = 2f;
	private float elapsedTime = 0;
	private Animator animator;

	public SwitchOff (CameraController g, Animator a)
	{
		paraApagar = g;
		animator = a;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	override public ActionState Execute ()
	{
		
		if (paraApagar.CompareTag ("PC")) 
		{
			paraApagar.TurnOffCamera();
		}
		if (waitTime > elapsedTime) {
			elapsedTime += 1 * Time.deltaTime;
			animator.SetBool ("Moving", false);
			animator.SetBool ("Waiting", true);
			return ActionState.Running;
		} else {
			animator.SetBool ("Waiting", false);
			return ActionState.Finished;
		}
	}
}
