﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : Actions 
{
	private DoorOpen door;

	public Key (DoorOpen d)
	{
		door = d;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	override public ActionState Execute ()
	{
		door.Open();
		return ActionState.Finished;
	}
}
