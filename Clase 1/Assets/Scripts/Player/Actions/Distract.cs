﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distract : Actions 
{
	[SerializeField]
	private ControladorNavMesh enemy;

	private Vector3 pos;

	public Distract (Vector3 v, ControladorNavMesh e)
	{
		pos = v;
		enemy = e;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	override public ActionState Execute()
	{
		enemy.ActualizarPuntoDestinoNavMeshAgent (pos);
		return ActionState.Finished;
	}

}
