﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionState
{
    None,
    Running,
    Finished,
}

public class Actions {

	public Actions (){
	}

	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		
	}
	public virtual ActionState Execute()
	{
        return ActionState.None;
	}

}
