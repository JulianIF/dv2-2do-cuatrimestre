﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCameras : MonoBehaviour 
{
	[SerializeField]
	private Camera ortographic;
	[SerializeField]
	private Camera other;
	[SerializeField]
	private Camera minimap;
	[SerializeField]
	private Camera enemy;
	// Use this for initialization
	void Start () 
	{
		if (ortographic)
			ortographic.enabled = true;
		if (other)
			other.enabled = false;
		if (minimap)
			minimap.enabled = false;
		if (enemy)
			enemy.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void ChangeCameras ()
	{
		if (ortographic.isActiveAndEnabled)
		{
			//Debug.Log ("sjidfngioasjdbfguob");
			ortographic.enabled = false;
			other.enabled = true;
		//	minimap.enabled = true;
//			enemy.enabled = true;
		}
		else if (other.isActiveAndEnabled)
		{
			other.enabled = false;
			ortographic.enabled = true;
			minimap.enabled = false;
			enemy.enabled = false;
		}
	}
}
