﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameState
{
    Planning,
    Playing,
}

public class PlayerController : MonoBehaviour {
	//public Animator animator;
	//MAIN CAMERA
	[SerializeField]
	private GameObject cam;

	[SerializeField]
	private GameObject camPos;

	//SECURITY CAMERAS CONTROLLER
	[SerializeField]
	private CameraController PC;
	//DOOR CONTROLLER
	[SerializeField]
	private DoorOpen door;

	//NAV MES CONTROLLER FOR ENEMY
	[SerializeField]
	private ControladorNavMesh enemyNavMesh;

	//LINE RENDERER
	[SerializeField]
	private Lines l;

	//ACTION LIST
	private Actions currentAction;
	public Queue<Actions> playerActionQueue = new Queue<Actions> ();
	private ActionState currentActionState = ActionState.Finished;

	//PLAYER'S EYES POSITION
    [SerializeField]
    private GameObject ojosPos;

	[SerializeField]
	SwitchCameras switchCameras;
    Vector3 newPos;
	Vector3 distractPos;
    Vector3 smokePos;

    public int actionCount;
   
    public GameObject prefab;
    public GameObject bomb;

    GameState state = GameState.Planning;

	private bool frentePC = false;
    public bool botonWait = false;
	public bool botonPC = false;
	public bool botonDistraer = false;
    public bool botonSmoke = false;
	public bool botonKey = false;
	private RaycastHit hitGoal;
	private bool lastAction = false;

	public enum PlanningState
	{
		Waypoint,
		Distract,
		Wait,
		PC,
        Smoke,
		Key,
		None
	}

	PlanningState planningState = PlanningState.None;

    void Start () 
	{
        prefab = Resources.Load("TargetPos") as GameObject;
		//animator = GetComponentInChildren<Animator>();
    }
	
	// Update is called once per frame
	void Update () 
	{

		//animator.SetBool ("Jugando", lastAction);
        switch (state)
        {
            case GameState.Planning:
                Plan();
                break;
            case GameState.Playing:
                //SIDE MOVEMENT (LIMITED)
                if (currentActionState == ActionState.Finished)
                {
                    setAction();
                }
                currentActionState = currentAction.Execute();
                //cam.transform.position = Vector3.Lerp(cam.transform.position, camPos.transform.position, 0.5f);
                break;
        }
	}

	void setAction()
	{
		if (playerActionQueue.Count > 0) 
		{
			currentAction = playerActionQueue.Dequeue ();
        }
	}

    void Plan()
    {
		switch (planningState) 
		{
			case PlanningState.Waypoint:
			PlanningWaypoints ();
			break;
			case PlanningState.Distract:
			PlanningDistraction();
			break;
			case PlanningState.Wait:
			PlanningWait ();
			break;
			case PlanningState.PC:
			PlanningPC ();
			break;
        	case PlanningState.Smoke:
            PlanningSmoke();
            break;
			case PlanningState.Key:
			PlanningKey ();
			break;
		}

		if (!lastAction)
        {
            if (Input.GetMouseButtonDown(0))
            {
				planningState = PlanningState.Waypoint;
            }
			if (Input.GetKeyDown(KeyCode.W) || botonWait) 
			{
				planningState = PlanningState.Wait;
			}
			if ((Input.GetKeyUp (KeyCode.D) || botonPC) && StaticVariables.hack) 
			{
				planningState = PlanningState.PC;
			}
			if ((Input.GetKeyDown (KeyCode.Q) || botonDistraer) && StaticVariables.rock) 
			{
				planningState = PlanningState.Distract;
			}
			if ((Input.GetKeyDown (KeyCode.B) || botonSmoke) && StaticVariables.smoke)
            {
                planningState = PlanningState.Smoke;
            }
			if ((Input.GetKeyDown (KeyCode.K) || botonKey) && StaticVariables.key) 
			{
				planningState = PlanningState.Key;
			}

        }
        else
        {
            state = GameState.Playing;
			switchCameras.ChangeCameras();
        }
    }

	private void PlanningWaypoints()
	{
		RaycastHit hitSuelo;
		RaycastHit hitObstacle;

		Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

		if (Physics.Raycast(ray, out hitSuelo))
		{
			Debug.Log (hitSuelo.collider.tag);

			if (hitSuelo.collider.CompareTag("Floor") || hitSuelo.collider.CompareTag("PC") || hitSuelo.collider.CompareTag("Goal") || hitSuelo.collider.CompareTag("Door")|| hitSuelo.collider.CompareTag("Price"))
			{
				newPos = hitSuelo.point;
				newPos.y = 2;
				if (hitSuelo.collider.CompareTag ("Goal")) 
				{
						lastAction = true;
				}

				if (Physics.Raycast(ojosPos.transform.position + Vector3.up * 2.0f, (newPos - ojosPos.transform.position + Vector3.up * 2.0f).normalized, out hitObstacle, Vector3.Distance(ojosPos.transform.position, newPos)))
				{
					if (hitObstacle.collider.CompareTag("Obstacle"))
					{
						Debug.Log("OBSTACULO");
					}
					else
					{
						Move m = new Move(newPos, ref camPos,GetComponentInChildren<Animator>());
						playerActionQueue.Enqueue(m);
						ojosPos.transform.position = newPos;
						l.SetTarget (newPos);
					}
				}
				else
				{
					Move m = new Move(newPos, ref camPos, GetComponentInChildren<Animator>());
					playerActionQueue.Enqueue(m);
					ojosPos.transform.position = newPos;
					l.SetTarget (newPos);
				}
			}
		}
		planningState = PlanningState.None;
	}
	public void PlanningWait()
	{
		botonWait = true;
		Wait w = new Wait (5, GetComponentInChildren<Animator>());
		playerActionQueue.Enqueue (w);
		botonWait = false;
		planningState = PlanningState.None;
	}
	public void PlanningPC()
	{
		botonPC = true;
		RaycastHit hitPC;
		if (Physics.Raycast (ojosPos.transform.position + Vector3.up * 5.0f, Vector3.down, out hitPC, 8)) 
		{
			SwitchOff s = new SwitchOff (PC, GetComponentInChildren<Animator>());
			playerActionQueue.Enqueue (s);
		}
		planningState = PlanningState.None;
		botonPC = false;
	}
	public void PlanningDistraction()
	{
		botonDistraer= true;
		if (Input.GetMouseButtonDown(0)) 
		{
			RaycastHit hitSuelo;

			Ray ray = cam.GetComponent<Camera> ().ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hitSuelo)) 
			{
				if (hitSuelo.collider.CompareTag ("Floor") || hitSuelo.collider.CompareTag ("PC")) 
				{
					distractPos = hitSuelo.point;
					GameObject targetPos = Instantiate (prefab) as GameObject;
					targetPos.transform.position = distractPos;
					Distract d = new Distract (distractPos, enemyNavMesh);
					playerActionQueue.Enqueue (d);
				}
			}
            planningState = PlanningState.None;
            botonDistraer = false;
        }
	}
	public void PlanningSmoke()
    {
		botonSmoke = true;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitSuelo;

            Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hitSuelo))
            {
                if (hitSuelo.collider.CompareTag("Floor") || hitSuelo.collider.CompareTag("PC"))
                {
                    smokePos = hitSuelo.point;
                    GameObject targetPos = Instantiate(prefab) as GameObject;
                    targetPos.transform.position = smokePos;
                    Smoke s = new Smoke(smokePos, bomb);
                    playerActionQueue.Enqueue(s);
                }
            }
            planningState = PlanningState.None;
            botonSmoke = false;
        }
    }
	public void PlanningKey ()
	{
		botonKey = true;
		RaycastHit hitKey;
		if (Physics.Raycast (ojosPos.transform.position + Vector3.up * 5.0f, Vector3.down, out hitKey, 8)) 
		{
			Key k = new Key (door);
			GameObject go = door.transform.GetChild (0).gameObject;
			go.tag = "Door";
			playerActionQueue.Enqueue (k);
		}
		planningState = PlanningState.None;
		botonKey = false;
	}
}
