﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonWait : MonoBehaviour 
{
	public PlayerController Player;

	Button button;

	// Use this for initialization
	void Start () {
		button = GetComponent<Button> ();

		button.onClick.AddListener (OnClick);
	}

	public void OnClick()
	{
		Player.botonWait = true;
	}
}
