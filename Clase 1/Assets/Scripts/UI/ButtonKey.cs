﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonKey : MonoBehaviour {

	public PlayerController Player;

	Button button;

	// Use this for initialization
	void Start () 
	{
		button = GetComponent<Button> ();
		if (!StaticVariables.key) 
		{
			button.interactable = false;
		}
		button.onClick.AddListener (OnClick);

	}

	public void OnClick()
	{
		Player.botonKey = true;
	}
}
