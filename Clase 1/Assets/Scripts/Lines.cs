﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lines : MonoBehaviour {

	private LineRenderer lineRenderer;

	[SerializeField]
	private GameObject player;

	// Use this for initialization
	void Start () 
	{
		lineRenderer = GetComponent<LineRenderer> ();
		//lineRenderer.SetPosition (0, player.transform.position);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SetTarget (Vector3 v)
	{
		if (lineRenderer.positionCount == 0) 
		{
			lineRenderer.positionCount++;
			lineRenderer.SetPosition (0, player.transform.position);
		} 

		lineRenderer.positionCount++;
		lineRenderer.SetPosition (lineRenderer.positionCount - 1, v);
	}
}
