﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameLogic : MonoBehaviour 
{

	public ChangeScene change;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void EndLevel ()
	{
		change.ChangeToScene ("PreLevelCustomization");
	}

	public void PlayerLost ()
	{
		EndLevel ();
		StaticVariables.currentLevel = 0;
	}
	public void EndMenu ()
	{
		if (SceneManager.GetActiveScene ().name == "MainMenu")
			change.ChangeToScene ("PreLevelCustomization");
		else 
		{
			StaticVariables.currentLevel += 1;
			change.ChangeToScene ("Nivel" + StaticVariables.currentLevel);
		}
	}
}
