﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour {

	public string nextScene;
	public GameLogic game;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter (Collider col)
	{
		if (col.CompareTag ("Player")) 
		{
			game.EndLevel ();
		}
	}
}
